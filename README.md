AoC 2020
========

<hr />

[★ Advent of Code 2020](https://adventofcode.com/2020) is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like.

[★ Advent of Code 2020](https://adventofcode.com/2020) is solved using [TypeScript](https://www.typescriptlang.org/).

<hr />

| Stars |                        Day                        |                                      Comments                                      |
|-------|---------------------------------------------------|------------------------------------------------------------------------------------|
| ⭐⭐    | [Day 01: Report Repair](doc/day01/day01.md)       | Easy. The most dificult part was to read the input file.                           |
| ⭐⭐    | [Day 02: Password Philosophy](doc/day02/day02.md) | Easy. The most dificult part was to create and import a Type.                      |
| ⭐⭐    | [Day 03: Toboggan Trajectory](doc/day03/day03.md) | Medium. To determine how many times the input map needs be repeated                |
| ⭐⭐    | [Day 04: Passport Processing](doc/day04/day04.md) | Easy. Process input (It was in multiple lines) and create validations.             |
| ⭐⭐    | [Day 05: Binary Boarding](doc/day05/day05.md)     | Easy. Implementation was easy, understanding what Part Two was asking for was not. |
| ⭐⭐    | [Day 06: Custom Customs](doc/day06/day06.md)      | Easy. Trying to use idiomatic TypeScript                                           |
|       | [Day 07: ](doc/day07/day07.md)                    |                                                                                    |
| ⭐     | [Day 08: Handheld Halting](doc/day08/day08.md)    | Easy.                                                                              |
|       | [Day 09: ](doc/day09/day09.md)                    |                                                                                    |
|       | [Day 10: ](doc/day10/day10.md)                    |                                                                                    |
|       | [Day 11: ](doc/day11/day11.md)                    |                                                                                    |
|       | [Day 12: ](doc/day12/day12.md)                    |                                                                                    |
|       | [Day 13: ](doc/day13/day13.md)                    |                                                                                    |
|       | [Day 14: ](doc/day14/day14.md)                    |                                                                                    |
|       | [Day 15: ](doc/day15/day15.md)                    |                                                                                    |
|       | [Day 16: ](doc/day16/day16.md)                    |                                                                                    |
|       | [Day 17: ](doc/day17/day17.md)                    |                                                                                    |
|       | [Day 18: ](doc/day18/day18.md)                    |                                                                                    |
|       | [Day 19: ](doc/day19/day19.md)                    |                                                                                    |
|       | [Day 20: ](doc/day20/day20.md)                    |                                                                                    |
|       | [Day 21: ](doc/day21/day21.md)                    |                                                                                    |
|       | [Day 22: ](doc/day22/day22.md)                    |                                                                                    |
|       | [Day 23: ](doc/day23/day23.md)                    |                                                                                    |
|       | [Day 24: ](doc/day24/day24.md)                    |                                                                                    |
|       | [Day 25: ](doc/day25/day25.md)                    |                                                                                    |


Usage
-----

```
./bin/run --help

./bin/run day01 --help

./bin/run day01 a doc/day01/day01.sample.in 

./bin/run day01 a < doc/day01/day01.sample.in 

cat doc/day01/day01.sample.in | ./bin/run day01 a
```

License
-------

MIT License

Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>

For doc/ directory contents. Copyright 2020 [/u/topaz2078 ](https://www.reddit.com/user/topaz2078)
