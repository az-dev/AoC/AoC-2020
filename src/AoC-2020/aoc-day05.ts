/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

/**
*
*/
export default class AoCDay05 {
  private data: Array<string>

  private debug: boolean

  constructor(data: Array<string>, debug = false) {
    this.debug = debug
    this.data = data
  }

  partA(): number {
    if (this.debug) {
      console.debug('Part A')
    }

    const seat_ids: Array<number> = []
    for (const boarding_pass of this.data) {
      seat_ids.push(this.decode(boarding_pass))
    }

    return Math.max(...seat_ids)
  }

  partB(): number {
    if (this.debug) {
      console.debug('Part B')
    }

    const seat_ids: Array<number> = []
    for (const boarding_pass of this.data) {
      seat_ids.push(this.decode(boarding_pass))
    }

    const max = Math.max(...seat_ids)
    const min = Math.min(...seat_ids)

    for (let i = min; i <= max; i++) {
      if (!seat_ids.includes(i)) {
        return i
      }
    }

    throw Object.assign(new Error("Solution doesn't exist"))
  }

  private decode(boarding_pass: string): number {
    let row = 0
    let col = 0

    let row_min = 0
    let row_max = 127
    let col_min = 0
    let col_max = 7

    for (const e of boarding_pass) {
      if (e === 'F') {
        row_max = row_min + Math.floor((row_max - row_min) / 2)
      } else if (e === 'B') {
        row_min += Math.ceil((row_max - row_min) / 2)
      } else if (e === 'L') {
        col_max = col_min + Math.floor((col_max - col_min) / 2)
      } else if (e === 'R') {
        col_min += Math.ceil((col_max - col_min) / 2)
      } else {
        throw Object.assign(new Error("Solution doesn't exist"))
      }

      if (this.debug) {
        console.log(`row_min = ${row_min}\nrow_max = ${row_max}\ncol_min = ${col_min}\ncol.max = ${col_max}\n`, '='.repeat(80))
      }
    }

    if (row_max !== row_min || col_max !== col_min) {
      throw Object.assign(new Error("Solution doesn't exist"))
    }

    row = row_max
    col = col_max

    return (row * 8) + col
  }
}
