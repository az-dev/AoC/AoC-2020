/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

/**
*
*/
export default class AoCDay03 {
  private data: Array<string>

  private debug: boolean

  constructor(data: Array<string>, debug = false) {
    this.debug = debug
    this.data = data
  }

  partA(): number {
    if (this.debug) {
      console.debug('Part A')
    }

    const right_movement = 3
    const down_movement = 1

    const map = this.buildMap(right_movement)
    const number_trees = this.traverseMap(map, right_movement, down_movement)

    return number_trees
  }

  partB(): number {
    if (this.debug) {
      console.debug('Part B')
    }

    const number_trees: Array<number> = []
    const movements = [
      [1, 1],
      [3, 1],
      [5, 1],
      [7, 1],
      [1, 2],
    ]

    for (const movement of movements) {
      const right_movement = movement[0]
      const down_movement = movement[1]

      const map = this.buildMap(right_movement)
      number_trees.push(this.traverseMap(map, right_movement, down_movement))

      if (this.debug) {
        console.debug('='.repeat(80))
      }
    }

    return number_trees.reduce((a, b) => a * b)
  }

  private traverseMap(map: Array<string>, right_movement: number, down_movement: number): number {
    let x = 0
    let y = 0
    let number_trees = 0

    if (this.debug) {
      console.debug('map', map)
      console.debug(`right_movement = ${right_movement}. down_movement = ${down_movement}`)
    }

    for (let i = 0; i < map.length - 1; i += down_movement) {
      x += down_movement
      y += right_movement
      const value = map[x][y]

      const line = [...map[x]]
      if (value === '#') {
        number_trees++
        line[y] = 'X'
      } else {
        line[y] = 'O'
      }
      map[x] = line.join('')

      if (this.debug) {
        console.debug(`map[${x}][${y}] => ${value}`)
        console.debug('map', map)
      }
    }

    return number_trees
  }

  private buildMap(right_movement: number): Array<string> {
    const height = this.data.length
    const width = this.data[0].length

    // height * right_movement + one extra right_movement
    // always add one extra chunk of the map
    const times_data = Math.floor(((height * right_movement) + right_movement) / width) + 1

    const map: Array<string> = []
    for (const line of this.data) {
      map.push(line.repeat(times_data))
    }

    if (this.debug) {
      console.debug(`height: ${height}. width: ${width}. required_size ${times_data}`)
    }

    return map
  }
}
