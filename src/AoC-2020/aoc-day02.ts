/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

export type PasswordPolicyType = {
  position1: number;
  position2: number;
  letter: string;
  password: string;
}

/**
*
*/
export default class AoCDay02 {
  private data: Array<PasswordPolicyType>

  private debug: boolean

  private password_valid: number

  constructor(data: Array<PasswordPolicyType>, debug = false) {
    this.debug = debug
    this.data = data
    this.password_valid = 0
  }

  partA(): number {
    if (this.debug) {
      console.debug('Part A')
    }

    for (const password_policy of this.data) {
      let i = 0

      for (const e of password_policy.password) {
        if (e === password_policy.letter) {
          i++
        }
      }

      if (password_policy.position1 <= i && i <= password_policy.position2) {
        this.password_valid++
      }
    }

    return this.password_valid
  }

  partB(): number {
    if (this.debug) {
      console.debug('Part B')
    }

    for (const password_policy of this.data) {
      const p = password_policy.password
      const l = password_policy.letter
      const p1 = password_policy.position1
      const p2 = password_policy.position2

      if ((p[p1 - 1] === l || p[p2 - 1] === l) && p[p1 - 1] !== p[p2 - 1]) {
        this.password_valid++
      }
    }

    return this.password_valid
  }
}
