/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

/**
*
*/
export default class AoCDay06 {
  private data: Array<Array<string>>

  private debug: boolean

  constructor(data: Array<Array<string>>, debug = false) {
    this.debug = debug
    this.data = data
  }

  partA(): number {
    if (this.debug) {
      console.debug('Part A')
    }

    const groups_questions: Array<number> = []

    for (const group of this.data) {
      const all = group.join('')
      const group_questions = {}

      for (const elem of [...all]) {
        if (group_questions[elem] === undefined) {
          group_questions[elem] = 0
        }
        group_questions[elem]++
      }

      groups_questions.push(Object.keys(group_questions).length)
    }

    return groups_questions.reduce((total, value) => {
      return total + value
    })
  }

  partB(): number {
    if (this.debug) {
      console.debug('Part B')
    }

    let total_similar_questions_per_group = 0

    for (const group of this.data) {
      const group_questions = {}

      for (const person of group) {
        for (const question of [...person]) {
          if (group_questions[question] === undefined) {
            group_questions[question] = 0
          }
          group_questions[question]++
        }
      }

      Object.entries(group_questions).forEach(([_key, value]) => {
        if (group.length === value) {
          total_similar_questions_per_group++
        }
      })
    }

    return total_similar_questions_per_group
  }
}
