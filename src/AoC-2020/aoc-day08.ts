/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

export class PC {
  debug: boolean

  accumulator: number

  lines: Record<string, any>

  error_on_loop: boolean

  constructor(error_on_loop: boolean = true,  debug: boolean = false) {
    this.debug = debug
    this.accumulator = 0
    this.lines = {}
    this.error_on_loop = error_on_loop
  }

  run(data: Array<string>): number {
    for (let i = 1; i <= data.length; i++) {
      const [instruction, argument] = this.parse(data[i - 1])

      switch (instruction) {
      case 'acc':
        this.accumulator += Number(argument)
        break
      case 'jmp':
        i += Number(argument) - 1
        break
      case 'nop':
        // No OPeration - just consume the line
        break
      default:
        throw Object.assign(new Error('Unkown instruction.'))
        break
      }

      if (this.debug) {
        console.debug(`line ${i}`)
        console.debug(`instruction ${instruction} - argument ${argument}`)
        console.debug(`accumulator ${this.accumulator}`)
      }

      if (this.lines.length > 0 && this.lines[i].status === 'executed') {
        if (this.error_on_loop) {
          if (this.debug) {
            console.debug(this.lines)
          }

          throw Object.assign(new Error(`Line already executed. Accumulator was '${this.accumulator}'`))
        } else {
          throw Object.assign(new Error(`Line already executed. Accumulator was '${this.accumulator}'... backtraking`))
        }
      } else {
        this.lines[i] = { 'instruction': instruction, 'argument': argument, 'accumulator': this.accumulator, 'status': 'executed' }
        if (this.debug) {
          console.debug(this.lines)
        }
      }

      if (this.debug) {
        console.debug('-'.repeat(80))
      }
    }

    return this.accumulator
  }

  private parse(line: string): Array<string> {
    return line.split(' ')
  }
}

/**
*
*/
export default class AoCDay08 {
  private data: Array<string>

  private debug: boolean

  constructor(data: Array<string>, debug = false) {
    this.debug = debug
    this.data = data
  }

  partA(): number {
    if (this.debug) {
      console.debug('Part A')
    }

    const pc = new PC(true, this.debug)
    const acc = pc.run(this.data)

    return acc
  }

  partB(): number {
    if (this.debug) {
      console.debug('Part B')
    }

    const pc = new PC(false, this.debug)
    const acc = pc.run(this.data)

    return acc
  }
}
