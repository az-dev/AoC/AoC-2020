/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

/**
*
*/
export default class AoCDay04 {
  private data: Array<Array<string>> = []

  private debug: boolean

  constructor(data: Array<string>, debug = false) {
    this.debug = debug

    for (const d of data) {
      const tokens = d.split(' ')

      const passport: Array<string> = []
      for (const token of tokens) {
        const parts = token.split(':')

        passport[parts[0]] = parts[1]
      }

      this.data.push(passport)
    }
  }

  partA(): number {
    if (this.debug) {
      console.debug('Part A')
    }

    let valid_passports = 0

    for (const passport of this.data) {
      if (
        ('byr' in passport) && ('iyr' in passport) && ('eyr' in passport) &&
        ('hgt' in passport) && ('hcl' in passport) && ('ecl' in passport) &&
        ('pid' in passport)
      ) {
        valid_passports++
      }
    }

    return valid_passports
  }

  /* eslint dot-notation: [2, {"allowPattern": "^[a-z]{3}$"}] */
  /* eslint-env es6 */
  partB(): number {
    if (this.debug) {
      console.debug('Part B')
    }

    let valid_passports = 0

    for (const passport of this.data) {
      if (
        ('byr' in passport && this.isBYRValid(passport['byr'])) &&
        ('iyr' in passport && this.isIYRValid(passport['iyr'])) &&
        ('eyr' in passport && this.isEYRValid(passport['eyr'])) &&
        ('hgt' in passport && this.isHGTValid(passport['hgt'])) &&
        ('hcl' in passport && this.isHCLValid(passport['hcl'])) &&
        ('ecl' in passport && this.isECLValid(passport['ecl'])) &&
        ('pid' in passport && this.isPIDValid(passport['pid']))
      ) {
        valid_passports++
      }
    }

    return valid_passports
  }

  private isBYRValid(data: string): boolean {
    return this.isNumber(data) && Number(data) >= 1920 && Number(data) <= 2002
  }

  private isIYRValid(data: string): boolean {
    return this.isNumber(data) && Number(data) >= 2010 && Number(data) <= 2020
  }

  private isEYRValid(data: string): boolean {
    return this.isNumber(data) && Number(data) >= 2020 && Number(data) <= 2030
  }

  private isHGTValid(data: string): boolean {
    const unit = data.slice(data.length - 2)
    const number = data.slice(0, data.length - 2)

    return this.isNumber(number) &&
      ((unit === 'cm' && Number(number) >= 150 && Number(number) <= 193) ||
      (unit === 'in' && Number(number) >= 59 && Number(number) <= 76))
  }

  private isHCLValid(data: string): boolean {
    const regexp = /^#[0-9a-f]{6}$/g
    return regexp.test(data)
  }

  private isECLValid(data: string): boolean {
    return ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(data)
  }

  private isPIDValid(data: string): boolean {
    return this.isNumber(data) && data.length === 9
  }

  private isNumber(value: string | number): boolean {
    return ((value !== null) && (value !== '') && !isNaN(Number(value.toString())))
  }
}
