/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

/**
*
*/
export default class AoCDay01 {
  private data: Array<number>

  private debug: boolean

  constructor(data: Array<number>, debug = false) {
    this.debug = debug
    this.data = data
  }

  partA(): number {
    if (this.debug) {
      console.debug('Part A')
    }

    for (let i = 0; i < this.data.length; i++) {
      for (let j = 0; j < this.data.length; j++) {
        const a = this.data[i]
        const b = this.data[j]
        const sum = a + b

        if (this.debug) {
          console.debug(`${a} + ${b} = ${sum}`)
        }

        if (sum === 2020) {
          const goal = a * b

          if (this.debug) {
            console.debug(`» ${a} * ${b} = ${goal}`)
          }

          return goal
        }
      }
    }

    throw Object.assign(new Error("Solution doesn't exist"))
  }

  /* eslint max-depth: ["error", 5] */
  /* eslint-env es6 */
  partB(): number {
    if (this.debug) {
      console.debug('Part B')
    }

    for (let i = 0; i < this.data.length; i++) {
      for (let j = 0; j < this.data.length; j++) {
        for (let k = 0; k < this.data.length; k++) {
          const a = this.data[i]
          const b = this.data[j]
          const c = this.data[k]
          const sum = a + b + c

          if (this.debug) {
            console.debug(`${a} + ${b} + ${c} = ${sum}`)
          }

          if (sum === 2020) {
            const goal = a * b * c

            if (this.debug) {
              console.debug(`» ${a} * ${b} * ${c} = ${goal}`)
            }

            return goal
          }
        }
      }
    }

    throw Object.assign(new Error("Solution doesn't exist"))
  }
}
