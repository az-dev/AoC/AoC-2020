/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

// dummy class to understand the framework organization

export default class AoCDay00 {
  debug: boolean

  constructor(debug = false) {
    this.debug = debug
  }

  partA() {
    if (this.debug) {
      console.debug('Part A')
    }

    return 21
  }

  partB() {
    if (this.debug) {
      console.debug('Part B')
    }

    return 42
  }
}
