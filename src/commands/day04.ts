/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {Command, flags} from '@oclif/command'
import AoCDay04 from '../AoC-2020/aoc-day04'

/**
* Class responsability is to parse arguments and flags, execute the program
* accordingly, gather input data either from a file or from standard input,
* process that data to feed it into the the class' day and print the answer.
*/
export default class Day04 extends Command {
  static description = 'Advent of Code - Day 04'

  static flags = {
    help: flags.help({char: 'h'}),
    debug: flags.boolean({char: 'd', default: false, allowNo: true}),
  }

  static args = [
    {
      name: 'part',
      required: true,
      description: 'The part of the puzzle to execute.',
      options: ['a', 'b'],
      default: 'a',
    },
    {
      name: 'file',
      required: false,
      description: 'The input data file.',
    },
  ]

  async run() {
    const {args, flags} = this.parse(Day04)
    const debug = flags.debug
    const file = args.file

    let data: Array<string>

    if (file) {
      data = this.getData(file)
    } else {
      data = this.getData(0)
    }

    const puzzle = new AoCDay04(data, debug)

    if (debug) {
      console.debug('Debug is enabled!')
    }

    if (args.part === 'a') {
      if (debug) {
        console.debug('Executing Part A.')
      }

      console.log(puzzle.partA())
    } else if (args.part === 'b') {
      if (debug) {
        console.debug('Executing Part B.')
      }

      console.log(puzzle.partB())
    }
  }

  private getData(fd): string[] {
    const fs = require('fs')

    const data: Array<string> = []
    const strings = fs.readFileSync(fd, 'utf-8').split('\n')

    let tmp: Array<string> = []
    for (const elem of strings) {
      if (elem === '') {
        data.push(tmp.join(' '))
        tmp = []
      } else {
        tmp.push(elem)
      }
    }

    return data
  }
}
