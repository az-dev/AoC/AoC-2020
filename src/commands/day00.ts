/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {Command, flags} from '@oclif/command'
import AoCDay00 from '../AoC-2020/aoc-day00'

// dummy class to understand the framework organization

export default class Day00 extends Command {
  static description = 'Advent of Code - Day 00'

  static flags = {
    help: flags.help({char: 'h'}),
    debug: flags.boolean({char: 'd', default: false, allowNo: true}),
  }

  static args = [
    {
      name: 'part',
      required: true,
      description: 'The part of the puzzle to execute.',
      options: ['a', 'b'],
      default: 'a',
    },
    {
      name: 'file',
      required: false,
      description: 'The input data file.',
    },
  ]

  async run() {
    const {args, flags} = this.parse(Day00)
    const debug = flags.debug

    const puzzle = new AoCDay00(debug)

    if (debug) {
      console.debug('Debug is enabled!')
    }

    if (args.part === 'a') {
      if (debug) {
        console.debug('Executing Part A.')
      }

      console.log(puzzle.partA())
    } else if (args.part === 'b') {
      if (debug) {
        console.debug('Executing Part B.')
      }

      console.log(puzzle.partB())
    }
  }
}
