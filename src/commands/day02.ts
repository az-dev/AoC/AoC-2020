/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {Command, flags} from '@oclif/command'
import AoCDay02 from '../AoC-2020/aoc-day02'
import type {PasswordPolicyType} from '../AoC-2020/aoc-day02'

/**
* Class responsability is to parse arguments and flags, execute the program
* accordingly, gather input data either from a file or from standard input,
* process that data to feed it into the the class' day and print the answer.
*/
export default class Day02 extends Command {
  static description = 'Advent of Code - Day 02'

  static flags = {
    help: flags.help({char: 'h'}),
    debug: flags.boolean({char: 'd', default: false, allowNo: true}),
  }

  static args = [
    {
      name: 'part',
      required: true,
      description: 'The part of the puzzle to execute.',
      options: ['a', 'b'],
      default: 'a',
    },
    {
      name: 'file',
      required: false,
      description: 'The input data file.',
    },
  ]

  async run() {
    const {args, flags} = this.parse(Day02)
    const debug = flags.debug
    const file = args.file

    let data: Array<PasswordPolicyType>

    if (file) {
      data = this.getData(file)
    } else {
      data = this.getData(0)
    }

    const puzzle = new AoCDay02(data, debug)

    if (debug) {
      console.debug('Debug is enabled!')
    }

    if (args.part === 'a') {
      if (debug) {
        console.debug('Executing Part A.')
      }

      console.log(puzzle.partA())
    } else if (args.part === 'b') {
      if (debug) {
        console.debug('Executing Part B.')
      }

      console.log(puzzle.partB())
    }
  }

  private getData(fd): PasswordPolicyType[] {
    const fs = require('fs')

    const data: Array<PasswordPolicyType> = []
    const strings = fs.readFileSync(fd, 'utf-8').split('\n')

    for (const elem of strings) {
      if (elem !== '') {
        const position1 = elem.split(' ')[0].split('-')[0]
        const position2 = elem.split(' ')[0].split('-')[1]
        const letter = elem.split(' ')[1].split(':')[0]
        const password = elem.split(' ')[2]

        const obj: PasswordPolicyType = {
          position1: Number(position1),
          position2: Number(position2),
          letter: letter,
          password: password,
        }

        data.push(obj)
      }
    }

    return data
  }
}
