/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {expect, test} from '@oclif/test'

// dummy test cases to understand the framework testing capabilities

describe('day00', () => {
  test
  .stdout()
  .command(['day00', '--debug', 'a', 'doc/sample_input.in'])
  .it('runs AoC Day 00 Part A', ctx => {
    expect(ctx.stdout).to.contain('21')
  })

  test
  .stdout()
  .command(['day00', '--debug', 'b', 'doc/sample_input.in'])
  .it('runs AoC Day 00 Part B', ctx => {
    expect(ctx.stdout).to.contain('42')
  })
})
