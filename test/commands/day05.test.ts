/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {expect, test} from '@oclif/test'

/**
* Test cases for sample data (which comes in the problem description) and
* for individual input data.
*
* Debug is enabled so that code coverage takes those lines into account.
*/
describe('day05', () => {
  test
  .stdout()
  .command(['day05', '--debug', 'a', 'doc/day05/day05.sample.in'])
  .it('runs AoC Day 05 Part A (sample data)', ctx => {
    expect(ctx.stdout).to.contain(820)
  })

  /*
  // There's NO sample input for Part B
  test
  .stdout()
  .command(['day05', '--debug', 'b', 'doc/day05/day05.sample.in'])
  .it('runs AoC Day 05 Part B (sample data)', ctx => {
    expect(ctx.stdout).to.contain(42)
  })
  */

  test
  .stdout()
  .command(['day05', '--debug', 'a', 'doc/day05/day05.in'])
  .it('runs AoC Day 05 Part A', ctx => {
    expect(ctx.stdout).to.contain(801)
  })

  test
  .stdout()
  .command(['day05', '--debug', 'b', 'doc/day05/day05.in'])
  .it('runs AoC Day 05 Part B', ctx => {
    expect(ctx.stdout).to.contain(597)
  })
})
