/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {expect, test} from '@oclif/test'

/**
* Test cases for sample data (which comes in the problem description) and
* for individual input data.
*
* Debug is enabled so that code coverage takes those lines into account.
*/
describe('day08', () => {
  test
  .stdout()
  .command(['day08', '--debug', 'a', 'doc/day08/day08.sample-a.in'])
  .it('runs AoC Day 08 Part A (sample data)', ctx => {
    expect(ctx.stdout).to.contain("Line already executed. Accumulator was '5'")
  })

  test
  .stdout()
  .command(['day08', '--debug', 'b', 'doc/day08/day08.sample-b.in'])
  .it('runs AoC Day 08 Part B (sample data)', ctx => {
    expect(ctx.stdout).to.contain(8)
  })

  test
  .stdout()
  .command(['day08', '--debug', 'a', 'doc/day08/day08.in'])
  .it('runs AoC Day 08 Part A', ctx => {
    expect(ctx.stdout).to.contain("Line already executed. Accumulator was '1563'")
  })

  /* test
  .stdout()
  .command(['day08', '--debug', 'b', 'doc/day08/day08.in'])
  .it('runs AoC Day 08 Part B', ctx => {
    expect(ctx.stdout).to.contain(42)
  }) */
})
