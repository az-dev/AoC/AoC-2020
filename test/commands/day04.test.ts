/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {expect, test} from '@oclif/test'

/**
* Test cases for sample data (which comes in the problem description) and
* for individual input data.
*
* Debug is enabled so that code coverage takes those lines into account.
*/
describe('day04', () => {
  test
  .stdout()
  .command(['day04', '--debug', 'a', 'doc/day04/day04.sample-a.in'])
  .it('runs AoC Day 04 Part A (sample data)', ctx => {
    expect(ctx.stdout).to.contain(2)
  })

  test
  .stdout()
  .command(['day04', '--debug', 'b', 'doc/day04/day04.sample-b.in'])
  .it('runs AoC Day 04 Part B (sample data)', ctx => {
    expect(ctx.stdout).to.contain(4)
  })

  test
  .stdout()
  .command(['day04', '--debug', 'a', 'doc/day04/day04.in'])
  .it('runs AoC Day 04 Part A', ctx => {
    expect(ctx.stdout).to.contain(245)
  })

  test
  .stdout()
  .command(['day04', '--debug', 'b', 'doc/day04/day04.in'])
  .it('runs AoC Day 04 Part B', ctx => {
    expect(ctx.stdout).to.contain(133)
  })
})
