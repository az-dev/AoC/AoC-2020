/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {expect, test} from '@oclif/test'

/**
* Test cases for sample data (which comes in the problem description) and
* for individual input data.
*
* Debug is enabled so that code coverage takes those lines into account.
*/
describe('day03', () => {
  test
  .stdout()
  .command(['day03', '--debug', 'a', 'doc/day03/day03.sample.in'])
  .it('runs AoC Day 03 Part A (sample data)', ctx => {
    expect(ctx.stdout).to.contain(7)
  })

  test
  .stdout()
  .command(['day03', '--debug', 'b', 'doc/day03/day03.sample.in'])
  .it('runs AoC Day 03 Part B (sample data)', ctx => {
    expect(ctx.stdout).to.contain(336)
  })

  test
  .stdout()
  .command(['day03', '--debug', 'a', 'doc/day03/day03.in'])
  .it('runs AoC Day 03 Part A', ctx => {
    expect(ctx.stdout).to.contain(237)
  })

  test
  .stdout()
  .command(['day03', '--debug', 'b', 'doc/day03/day03.in'])
  .it('runs AoC Day 03 Part B', ctx => {
    expect(ctx.stdout).to.contain(2106818610)
  })
})
