/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {expect, test} from '@oclif/test'

/**
* Test cases for sample data (which comes in the problem description) and
* for individual input data.
*
* Debug is enabled so that code coverage takes those lines into account.
*/
describe('day01', () => {
  test
  .stdout()
  .command(['day01', '--debug', 'a', 'doc/day01/day01.sample.in'])
  .it('runs AoC Day 01 Part A (sample data)', ctx => {
    expect(ctx.stdout).to.contain('514579')
  })

  test
  .stdout()
  .command(['day01', '--debug', 'b', 'doc/day01/day01.sample.in'])
  .it('runs AoC Day 01 Part B (sample data)', ctx => {
    expect(ctx.stdout).to.contain('241861950')
  })

  test
  .stdout()
  .command(['day01', '--debug', 'a', 'doc/day01/day01.in'])
  .it('runs AoC Day 01 Part A', ctx => {
    expect(ctx.stdout).to.contain('1007331')
  })

  test
  .stdout()
  .command(['day01', '--debug', 'b', 'doc/day01/day01.in'])
  .it('runs AoC Day 01 Part B', ctx => {
    expect(ctx.stdout).to.contain('48914340')
  })
})
