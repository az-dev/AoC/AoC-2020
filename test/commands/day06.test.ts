/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {expect, test} from '@oclif/test'

/**
* Test cases for sample data (which comes in the problem description) and
* for individual input data.
*
* Debug is enabled so that code coverage takes those lines into account.
*/
describe('day06', () => {
  test
  .stdout()
  .command(['day06', '--debug', 'a', 'doc/day06/day06.sample.in'])
  .it('runs AoC Day 06 Part A (sample data)', ctx => {
    expect(ctx.stdout).to.contain(11)
  })

  test
  .stdout()
  .command(['day06', '--debug', 'b', 'doc/day06/day06.sample.in'])
  .it('runs AoC Day 06 Part B (sample data)', ctx => {
    expect(ctx.stdout).to.contain(6)
  })

  test
  .stdout()
  .command(['day06', '--debug', 'a', 'doc/day06/day06.in'])
  .it('runs AoC Day 06 Part A', ctx => {
    expect(ctx.stdout).to.contain(6799)
  })

  test
  .stdout()
  .command(['day06', '--debug', 'b', 'doc/day06/day06.in'])
  .it('runs AoC Day 06 Part B', ctx => {
    expect(ctx.stdout).to.contain(3354)
  })
})
