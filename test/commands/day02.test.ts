/*
* MIT License - Copyright (c) 2020 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
*/

import {expect, test} from '@oclif/test'

/**
* Test cases for sample data (which comes in the problem description) and
* for individual input data.
*
* Debug is enabled so that code coverage takes those lines into account.
*/
describe('day02', () => {
  test
  .stdout()
  .command(['day02', '--debug', 'a', 'doc/day02/day02.sample.in'])
  .it('runs AoC Day 02 Part A (sample data)', ctx => {
    expect(ctx.stdout).to.contain(2)
  })

  test
  .stdout()
  .command(['day02', '--debug', 'b', 'doc/day02/day02.sample.in'])
  .it('runs AoC Day 02 Part B (sample data)', ctx => {
    expect(ctx.stdout).to.contain(1)
  })

  test
  .stdout()
  .command(['day02', '--debug', 'a', 'doc/day02/day02.in'])
  .it('runs AoC Day 02 Part A', ctx => {
    expect(ctx.stdout).to.contain(524)
  })

  test
  .stdout()
  .command(['day02', '--debug', 'b', 'doc/day02/day02.in'])
  .it('runs AoC Day 02 Part B', ctx => {
    expect(ctx.stdout).to.contain(485)
  })
})
